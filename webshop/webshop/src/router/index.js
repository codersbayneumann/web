import {createRouter, createWebHistory} from 'vue-router'
import RegisterView from "@/views/RegisterView.vue";
import LoginView from "@/views/LoginView.vue";
import PostProductView from "@/views/PostProductView.vue";
import {useAuthStore} from "@/stores/authStore";
import HomeView from "@/views/HomeView.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/register',
            name: 'register',
            component: RegisterView
        },
            {
            path: '/login',
            name: 'login',
            component: LoginView
            },
        {
            path: '/home',
            name: 'home',
            component: HomeView
        },
        {
            path: '/new-product',
            name: 'new-product',
            component: PostProductView
        }
    ]
})

/**
 * Diese Funktion wird beim initialen Seitenaufruf und bei jedem weiteren Seitenwechsel aufgerufen.
 * In diesem Fall werden einmal die Benutzerdaten geladen, damit die benutzerabhängigen Anzeigen (Button zum Warenkorb)
 * und späteren RouteGuards korrekt ausgeführt werden können.
 * https://router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards
 */
router.beforeEach(() => useAuthStore().initialize())


export default router
