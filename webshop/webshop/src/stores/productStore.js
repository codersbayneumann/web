import { defineStore } from 'pinia'
import axios from "axios";
import {computed} from "vue";

const defaultProducts = [
    {
        id: 1,
        name: "Apple",
        price: 1.00
    },
    {
        id: 2,
        name: "Milk",
        price: 2.99
    },
    {
        id: 3,
        name: "Bread",
        price: 4.50
    },

]

export const useProductStore = defineStore('product', {
    actions:{

        async createNewProduct(productData){

            const createProductResponse = await axios
                .post('https://iyoitkeabx.webshop.asw.rest/api/products',
                    productData);

            this.products.push(createProductResponse.data);

            console.log("received response from POST-Request, data:", createProductResponse.data)
        },

        async getAllProducts(){

            const getAllProdResponse = await axios
                .get('https://iyoitkeabx.webshop.asw.rest/api/products');

            this.products = getAllProdResponse.data;

            console.log("received response from GET-Request, data:", getAllProdResponse.data);

            console.log("this.products is now, data:", this.products)
        },

        //
        //
        // async getSingleProductById(productId){
        //
        //     const getSingleProductResponse = await axios
        //         .get('https://pjxcpqkygh.webshop.asw.rest/api/products/' + productId);
        //
        //     // push the new userData to user array
        //     this.products.push(getSingleProductResponse.data);
        //     console.log("made get-request to receive product data, data: ", getSingleProductResponse.data)
        // },
        //
        // async checkProductDataExists(productId) {
        //
        //     // check if the prod. with this productId is already in local prod. array
        //     const foundProductData = this.products.find(product => product.productId === productId);
        //
        //     // if the product is not found, make a get-request to the api
        //     if (foundProductData === undefined) {
        //
        //         await this.getSingleProductById(productId);
        //
        //     } else console.log("Data exists in local products array.");
        // },
        //
        //
        // async editProductData(productId, changedProductData){
        //
        //     const editProductResponse = await axios
        //         .put('https://pjxcpqkygh.webshop.asw.rest/api/products/' + productId,
        //             changedProductData);
        //
        //     // replace data in local products array
        //     // const productIndex = this.products.findIndex(product => product.productId === productId);
        //     //
        //     // this.products.splice(productIndex, 1, editProductResponse.data);
        //
        //     this.products = editProductResponse.data;
        //     console.log("Edit Product: received data from put request, data:", editProductResponse.data);
        //
        //
        // },
        //
        // async deleteProduct(productId){
        //
        //     try {
        //         await axios.delete('https://pjxcpqkygh.webshop.asw.rest/api/products/' + productId);
        //
        //         // delete product in local products array
        //         const productIndex = this.products.findIndex(product => product.productId === productId);
        //
        //         this.products.splice(productIndex, 1);
        //
        //         console.log("Product with id: " + productId + " deleted.");
        //
        //     } catch (error) {
        //         if (error.response && error.response.status === 409) {
        //             console.error("Produkt befindet sich in einem Warenkorb eines Users." +
        //                 " Produkt kann nicht gelöscht werden.", error);
        //
        //             return false;
        //
        //         }
        //
        //     }
        //     return true;
        //
        // }


        //
        // async deleteSingleUser(userId){
        //
        //     // delete request auf /api/users/<userId>
        //     const deleteUserResponse =  await axios
        //         .delete('https://txkwbxeput.user-management.asw.rest/api/users/' + userId);
        //
        //     const userIndex = this.users.findIndex(user => user.userId === userId);
        //
        //     // delete user from local user array
        //     this.users.splice(userIndex, 1);
        //
        //     console.log("(delete user) received response from delete request, user deleted.");
        // }

        // async editUserData(userId, changedUserData ){
        //
        //     // put request auf /api/users/<userId>
        //     const editUserResponse = await axios
        //         .put('https://txkwbxeput.user-management.asw.rest/api/users/' + userId,
        //             changedUserData);
        //
        //     const userIndex = this.users.findIndex(user => user.userId === userId);
        //
        //     // ersetze user an der id mit userDTO aus der response
        //     this.users.splice(userIndex, 1, editUserResponse.data);
        //
        //     console.log("(edit user) received response from put request, data: ", editUserResponse.data);
        // },


        // async getAllUsers(){
        //
        //     const getAllUsersResponse = await axios
        //         .get('https://txkwbxeput.user-management.asw.rest/api/users');
        //
        //     this.users = getAllUsersResponse.data;
        // },
        //

        //




    },
    state: () => ({
        products: [...defaultProducts]
    })
})
