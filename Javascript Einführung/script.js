// Aufgabe 1

const button = document.getElementById('countButton');
let count = 1;
button.addEventListener('click', function () {
    document.getElementById("counter").innerHTML = count;
    count++
});

// Aufgabe 2

const addButton = document.getElementById("addButton");
const subtractButton = document.getElementById("subtractButton");
const multiplicationButton = document.getElementById("multiplicationButton");
const divisionButton = document.getElementById("divisionButton");

const num1Field = document.getElementById("num1");
const num2Field = document.getElementById("num2");
const resultField = document.getElementById("result");

addButton.addEventListener('click', function () {
    let num1 = parseInt(num1Field.value)
    let num2 = parseInt(num2Field.value)
    resultField.innerHTML = add(num1, num2);
});
subtractButton.addEventListener('click', function () {
    let num1 = parseInt(num1Field.value)
    let num2 = parseInt(num2Field.value)
    resultField.innerHTML = sub(num1, num2);
});
multiplicationButton.addEventListener('click', function () {
    let num1 = parseInt(num1Field.value)
    let num2 = parseInt(num2Field.value)
    resultField.innerHTML = mul(num1, num2);
});
divisionButton.addEventListener('click', function () {
    let num1 = parseInt(num1Field.value)
    let num2 = parseInt(num2Field.value)
    resultField.innerHTML = div(num1, num2);
});

function add(num1, num2) {
    return num1 + num2;
}

function sub(num1, num2) {
    return num1 - num2;
}

function mul(num1, num2) {
    return num1 * num2;
}

function div(num1, num2) {
    return num1 / num2;
}

// Aufgabe 3

const textTransportButton = document.getElementById("textTransportButton")
const textResetButton = document.getElementById("textResetButton")
textTransportButton.addEventListener('click', function () {
    let textAddition = document.getElementById("textInput").value;
    document.getElementById("textTransportResult").innerHTML += textAddition;
    document.getElementById("textInput").value = "";
});
textResetButton.addEventListener('click', function () {
    document.getElementById("textTransportResult").innerHTML = "";
});

//Bonusaufgabe

const colorMixButton = document.getElementById("colorMixButton");
let mixedColors = document.getElementById("mixedColor");
colorMixButton.addEventListener('click', function(){
   let red = document.getElementById("red").checked ? "F" : "0";
   let green = document.getElementById("green").checked ? "F" : "0";
   let blue = document.getElementById("blue").checked ? "F" : "0";
   mixedColors.style.backgroundColor = "#" + red + green + blue;
});
