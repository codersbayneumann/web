import { defineStore } from 'pinia'

export const usePersonStore = defineStore('person', {
    state: () => ({
        persons: [], // hier werden Personen gespeichert
    })
})
