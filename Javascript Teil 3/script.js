const personTableBody = document.getElementById('personTableBody');
const sortTableButtons = Array.from(document.getElementsByClassName('sort-table'));
const printAllPersonsButton = document.getElementById('printAllPersons');

let persons = [
    createPersonObject('Maria', 'Huber', 1990),
    createPersonObject('Franz', 'Müller', 1978),
    createPersonObject('Gerhard', 'Gruber', 1991),
    createPersonObject('Alina', 'Steiner', 1997)
];
render();

printAllPersonsButton.addEventListener("click", () => {
    const summary = persons.map(person => {
        return `${person.firstName} ${person.lastName} (${person.age})`
    }).join("\r\n")
    window.alert(summary);
})

personTableBody.addEventListener("click", (event) => {
    if (event.target.classList.contains("delete-person")) {
        let selectedFirstName = event.target.dataset.firstName;
        let selectedLastName = event.target.dataset.lastName;
        let selectedIndex = (persons.findIndex((person) => {
            return person.firstName === selectedFirstName && person.lastName === selectedLastName;
        }));
        if (window.confirm("Möchten Sie " + selectedFirstName + " " + selectedLastName + " wirklich löschen?")) {
            persons.splice(selectedIndex, 1);
            render();
        }
    }
});

sortTableButtons.forEach((button) => {
    button.addEventListener("click", (event) => {
        const sortAttribute = event.currentTarget.dataset.sortKey
        if (typeof persons[0][sortAttribute] === "number") {
            persons.sort((a, b) => {
                return b[sortAttribute] - a[sortAttribute]
            })
        } else if (typeof persons[0][sortAttribute] === "string") {
            persons.sort((a, b) => {
                return a[sortAttribute].toLowerCase() < b[sortAttribute].toLowerCase() ? -1 : 1
            })
        }
        render()
    })
})

function createPersonObject(firstName, lastName, birthYear) {
    return {
        firstName,
        lastName,
        birthYear,
        get age() {
            return new Date().getFullYear() - parseInt(this.birthYear)
        },
        set age(age) {
            this.birthYear = new Date().getFullYear() - age;
        }
    };
}

// ************************************************************
// AB HIER NICHTS ÄNDERN
// ************************************************************
const addPersonForm = document.getElementById('addPersonForm');
const addPersonFirstName = document.getElementById('addPersonFirstName');
const addPersonLastName = document.getElementById('addPersonLastName');
const addPersonBirthYear = document.getElementById('addPersonBirthYear');
const clearAllButton = document.getElementById('clearAllButton');

addPersonForm.addEventListener('submit', event => {
    event.preventDefault();

    const firstName = addPersonFirstName.value;
    const lastName = addPersonLastName.value;
    const birthYear = parseInt(addPersonBirthYear.value);

    persons.push(createPersonObject(firstName, lastName, birthYear));

    addPersonFirstName.value = '';
    addPersonLastName.value = '';
    addPersonBirthYear.value = '';

    render();
});

clearAllButton.addEventListener('click', () => {
    persons = [];
    render();
});

personTableBody.addEventListener('click', event => {
    if (event.target.classList.contains('update-age')) {
        let age = NaN;
        for (let i = 0; i < 3 && isNaN(age); i++) {
            age = parseInt(prompt('Geben Sie das neue Alter an'));
        }
        if (!isNaN(age)) {
            const personIndex = parseInt(event.target.dataset.index);
            persons[personIndex].age = age;
        }
    }
    render();
});

function render() {
    personTableBody.innerHTML = '';
    persons.forEach((person, index) => {
        let deletePersonButton = `<button type="button" class="btn btn-danger btn-sm delete-person" data-first-name="${person.firstName}" data-last-name="${person.lastName}">`;
        deletePersonButton += 'Löschen';
        deletePersonButton += '</button>';

        let updatePersonButton = `<button type="button" class="btn btn-warning btn-sm update-age" data-index="${index}">`;
        updatePersonButton += 'Alter ändern'
        updatePersonButton += '</button>'

        let personTableRow = '<tr>';
        personTableRow += `<td>${person.firstName}</td>`;
        personTableRow += `<td>${person.lastName}</td>`;
        personTableRow += `<td>${person.age} (Geburtsjahr: ${person.birthYear})</td>`;
        personTableRow += `<td class="text-end">${updatePersonButton} ${deletePersonButton}</td>`;
        personTableRow += '</tr>';
        personTableBody.innerHTML += personTableRow;
    });
}