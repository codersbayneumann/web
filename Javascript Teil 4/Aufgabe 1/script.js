const cursor = document.getElementById("cursor")
const links = document.querySelectorAll("a")
// 1. "mousemove" events are useful for tracking mouse movement.

// 2. & 4. Implementation of a mousemove event for the whole document:
document.addEventListener("mousemove", (e) => {
    cursor.setAttribute("style", `left: ${e.pageX}px; top: ${e.pageY}px`)
})
// 3. A visual explanation for differences regarding pageX/Y, client X/Y, screenX/Y is as "pageXYclientXYscreenXY.png" in this directory.

// 5.
links.forEach(
    function (link) {
        link.addEventListener("mouseover", () => {
                cursor.classList.add("cursor-small")
            }
        )
        link.addEventListener("mouseout",  () => {
                cursor.classList.remove("cursor-small")
        })
    }
)
