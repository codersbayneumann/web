const personTableBody = document.getElementById('personTableBody')
const contextMenuElement = document.getElementById('contextMenu')
const deletePersonButton = document.getElementById('deletePerson')
const changeAgeButton = document.getElementById('changeAge')

let selectedPersonIndex = 0

personTableBody.addEventListener("contextmenu", function (event) {
    event.preventDefault()
    contextMenuElement.classList.add("show")
    contextMenuElement.setAttribute("style", `right: ${window.innerWidth - event.pageX}px; top: ${event.pageY}px`)

    const targetIndex = event.composedPath().find(function (element) {
        return element.tagName === "TR";
    }).dataset.index
    selectedPersonIndex = parseInt(targetIndex)
})
document.addEventListener("click", () => {
    contextMenuElement.classList.remove("show")
})

deletePersonButton.addEventListener("click", () => {
    persons.splice(selectedPersonIndex, 1)
    render()
})
changeAgeButton.addEventListener("click", () => {
    let age;
    age = parseInt(prompt('Geben Sie das neue Alter an'))
    persons[selectedPersonIndex].age = age
    render()
})

// ************************************************************
// AB HIER NICHTS ÄNDERN
// ************************************************************
const addPersonForm = document.getElementById('addPersonForm');
const addPersonFirstName = document.getElementById('addPersonFirstName');
const addPersonLastName = document.getElementById('addPersonLastName');
const addPersonBirthYear = document.getElementById('addPersonBirthYear');
const clearAllButton = document.getElementById('clearAllButton');

let persons = [
    createPersonObject('Maria', 'Huber', 1990),
    createPersonObject('Franz', 'Müller', 1978),
    createPersonObject('Gerhard', 'Gruber', 1991),
    createPersonObject('Alina', 'Steiner', 1997)
];
render();

addPersonForm.addEventListener('submit', event => {
    event.preventDefault();

    const firstName = addPersonFirstName.value;
    const lastName = addPersonLastName.value;
    const birthYear = parseInt(addPersonBirthYear.value);

    persons.push(createPersonObject(firstName, lastName, birthYear));

    addPersonFirstName.value = '';
    addPersonLastName.value = '';
    addPersonBirthYear.value = '';

    render();
});

clearAllButton.addEventListener('click', () => {
    persons = [];
    render();
});

function createPersonObject(firstName, lastName, birthYear) {
    return {
        firstName,
        lastName,
        birthYear,
        get age() {
            return new Date().getFullYear() - this.birthYear
        },
        set age(age) {
            this.birthYear = new Date().getFullYear() - age
        }
    };
}

function render() {
    personTableBody.innerHTML = '';
    persons.forEach((person, index) => {
        let personTableRow = `<tr data-index="${index}">`;
        personTableRow += `<td>${person.firstName}</td>`;
        personTableRow += `<td>${person.lastName}</td>`;
        personTableRow += `<td>${person.age} (<span class="bi-calendar-heart"></span> ${person.birthYear})</td>`;
        personTableRow += '</tr>';
        personTableBody.innerHTML += personTableRow;
    });
}