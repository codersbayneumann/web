let personArray = []

const form = document.getElementById("personForm");
const firstNameInput = document.getElementById("firstName");
const lastNameInput = document.getElementById("lastName");
const personTableBody = document.getElementById("personTableBody");
const sortByLastNameButton = document.getElementById("sortByLastNameButton");
const sortByAgeButton = document.getElementById("sortByAgeButton");
const deleteLastElementButton = document.getElementById("deleteLastElementButton")
const deleteAllButton = document.getElementById("deleteAllButton")

form.addEventListener("submit", function (event) {
    event.preventDefault();

    const birthYearInput = document.getElementById("birthYear");
    const firstName = firstNameInput.value;
    const lastName = lastNameInput.value;
    const age = getAge(birthYearInput.value);
    const person = {firstName, lastName, age};

    personArray.push(person);
    render();

    firstNameInput.value = "";
    lastNameInput.value = "";
    birthYearInput.value = "";
});
sortByAgeButton.addEventListener("click", function(){
   let sortedPersonArray = personArray.sort(function(a, b) {
      return b.age - a.age;
   });
    personArray.push(sortedPersonArray);
    render(sortedPersonArray)
});
sortByLastNameButton.addEventListener("click", function(){
    let sortedPersonArray = personArray.sort(function(a, b){
        return a.lastName.toUpperCase() < b.lastName.toUpperCase() ? -1 : 1
    })
    personArray.push(sortedPersonArray);
    render(sortedPersonArray)
});
deleteLastElementButton.addEventListener("click", function(){
    personTableBody.deleteRow(-1);
    personArray.pop();
});
deleteAllButton.addEventListener("click",function(event) {
    personTableBody.innerHTML = "";
    personArray = [];
});

function getAge(birthYearInput) {
    return new Date().getFullYear() - parseInt(birthYearInput);
}
function render() {
    personTableBody.innerHTML = "";

    personArray.forEach((person) => {
        const dataRow = personTableBody.insertRow();
        const cell1 = dataRow.insertCell(0);
        const cell2 = dataRow.insertCell(1);
        const cell3 = dataRow.insertCell(2);

        cell1.textContent = person.firstName;
        cell2.textContent = person.lastName;
        cell3.textContent = person.age;
    });
}