// JavaScript to randomize the order of options in each dropdown
function shuffleOptions(selectId) {
    const select = document.getElementById(selectId);
    const options = Array.from(select.options);
    options.sort(() => Math.random() - 0.5);
    select.innerHTML = '';
    options.forEach(option => select.appendChild(option));
}

// Randomize the order of options for each dropdown
shuffleOptions('day1');
shuffleOptions('day2');
shuffleOptions('month1');
shuffleOptions('month2');
shuffleOptions('year1');
shuffleOptions('year2');
shuffleOptions('year3');
shuffleOptions('year4');

function initPhoneNumberInput() {
    const numberParts = document.querySelectorAll("#numParagraph span")
    const stopButtons = document.querySelectorAll("#stopButtons button")
    let stoppedNumberParts = [false, false, false]

    window.setInterval(function () {
        numberParts.forEach((el, i) => {
            const isStopped = stoppedNumberParts[i]
            const digitCount = parseInt(el.dataset.digits)

            if (!isStopped) {
                let n = ""
                for (let i = 0; i < digitCount; i++) {
                    n += Math.floor(Math.random() * 10)
                }
                el.innerText = n
            }
        })
    }, 100)

    stopButtons.forEach((el, i) => {
        el.addEventListener("click", () => {
            stoppedNumberParts[i] = !stoppedNumberParts[i]
        })
    })
}

initPhoneNumberInput()

let toggleOne = document.getElementById("toggleOne")
let toggleTwo = document.getElementById("toggleTwo")
let toggleThree = document.getElementById("toggleThree")

toggleOne.addEventListener("click", toggleButtonText)
toggleTwo.addEventListener("click", toggleButtonText)
toggleThree.addEventListener("click", toggleButtonText)

// Function to toggle the text of the clicked button
function toggleButtonText(event) {
    let button = event.target;
    if (button.innerHTML === "stop") {
        button.innerHTML = "go";
    } else {
        button.innerHTML = "stop";
    }
}
const submitButton = document.querySelector(".submit");
submitButton.addEventListener("click", event => {
    event.preventDefault();
    window.confirm("GEWONNEN! BITTE GEBEN SIE IHRE DATEN ERNEUT EIN");
    window.alert("Bitte DRÜCKEN SIE IHRE CTRL+F5 TASTENKOMBINATION");
})

